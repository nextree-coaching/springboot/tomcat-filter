package nextree.io.whitelist2.config;

import org.apache.catalina.filters.RemoteAddrFilter;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigTomcatAccess {
    //
    @Bean
    public FilterRegistrationBean remoteAddressFilter1() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();
        RemoteAddrFilter filter = new RemoteAddrFilter();
        filter.setAllow("172\\.32\\.0\\.([01]?\\d?\\d|2[0-4]\\d|25[0-5])");
//        filter.setDeny("172\\.32\\.0\\.([01]?\\d?\\d|2[0-4]\\d|25[0-5])");
        filterRegistrationBean.setFilter(filter);
        filterRegistrationBean.addUrlPatterns("/test");
        return filterRegistrationBean;
    }
}
