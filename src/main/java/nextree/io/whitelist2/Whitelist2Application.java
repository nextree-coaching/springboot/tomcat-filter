package nextree.io.whitelist2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Whitelist2Application {

    public static void main(String[] args) {
        SpringApplication.run(Whitelist2Application.class, args);
    }

}
