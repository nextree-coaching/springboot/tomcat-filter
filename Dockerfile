FROM openjdk:8-jdk-alpine
VOLUME /tmp
COPY build/libs/whitelist2-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java",  "-Duser.timezone='Asia/Seoul'", "-Xmx256m", "-jar","/app.jar"]
